/**
 * Created by Daniel on 2015/10/24.
 */


var {
    Styles,
    FlatButton,
    RaisedButton,
    TextField,
    Dialog
    } = MUI;

var { ThemeManager, LightRawTheme } = Styles;

injectTapEventPlugin();

// App component - represents the whole app
Login = React.createClass({

    childContextTypes: {
        muiTheme: React.PropTypes.object
    },

    getChildContext() {
        return {
            muiTheme: ThemeManager.getMuiTheme(LightRawTheme)
        };
    },

    // This mixin makes the getMeteorData method work
    mixins: [ReactMeteorData],

    // Loads items from the Tasks collection and puts them on this.data.tasks
    getMeteorData() {
        return {

        }
    },

    getInitialState() {
        return {
            requireField : "This field is required"
        }
    },

    submitHandler(event) {
        event.preventDefault();
        var username = this.refs.username.getValue().trim();
        var password = this.refs.password.getValue().trim();
        Meteor.loginWithPassword(username, password, function (err) {
            if (err) {
                console.log("wrong");
            } else {
                FlowRouter.reload();
                console.log("correct");
            }
            return false;
        })

    },

    checkInput(input) {
        if (input.search(/[^a-zA-Z0-9]/) != -1) {
            return 1;
        } else if (input.length < 3) {
            return 2;
        } else {
            return 0;
        }
    },

    onInput1Change() {
        var username = this.refs.username.getValue();
        if (username.search(/[^a-zA-Z0-9]/) != -1) {
            this.refs.username.state.errorText = "Only alphanumeric is allowed";
            this.refs.username.props.errorStyle = {color : 'orange'}
        } else if (username.length > 0) {
            this.refs.username.state.errorText = "";
            this.refs.username.props.errorStyle = {color : 'green'}
        } else {
            this.refs.username.state.errorText = this.state.requireField;
            this.refs.username.props.errorStyle = {color : 'red'}
        }
    },

    onInput2Change() {
        var password = this.refs.password.getValue();
        if (password.search(/[^a-zA-Z0-9]/) != -1) {
            this.refs.password.state.errorText = "Only alphanumeric is allowed";
            this.refs.password.props.errorStyle = {color : 'orange'}
        } else if (password.length > 0) {
            this.refs.password.state.errorText = "";
            this.refs.password.props.errorStyle = {color : 'green'}
        } else {
            this.refs.password.state.errorText = this.state.requireField;
            this.refs.password.props.errorStyle = {color : 'red'}
        }
    },

    registerHandler(event) {
        event.preventDefault();
        window.open('http://www.promise.com/tw/SignUp');
        var username = this.refs.username.getValue();
        var password = this.refs.password.getValue();
        if (this.checkInput(password) == 1) {
            console.log("no special char");
        } else if (this.checkInput(password) == 2) {
            console.log("not long enough");
        } else {
            //Accounts.createUser({
            //    username: username,
            //    password: password
            //}, function(err) {
            //    if (err) {
            //        console.log("Can't create user");
            //    } else {
            //        FlowRouter.reload();
            //    }
            //    return false;
            //});
        }
    },

    render() {
        return (
            <center className="loginForm">
                <form onSubmit={ this.submitHandler } >
                    <TextField ref="username"
                               floatingLabelText="Username"
                               errorStyle={ {color:'red'} }
                               errorText = { this.state.requireField }
                               onChange = { this.onInput1Change }
                        /><br/>
                    <TextField ref="password"
                               floatingLabelText="Password"
                               type="password"
                               errorStyle={ {color:'red'} }
                               errorText = { this.state.requireField }
                               onChange = { this.onInput2Change }
                        /><br/>
                    <FlatButton label="Login" type="submit"/>
                    <FlatButton label="Register" onTouchTap= { this.registerHandler } />
                </form>
            </center>
        );
    }
});