/**
 * Created by Daniel on 2015/10/23.
 */


Layout = React.createClass({
    render() {
        return (
            <div>
                <Header />
                <main> { this.props.content } </main>
                <Footer />
            </div>
        );
    }
});

Header = React.createClass({
    render() {
        return (
            <center>
                <header>
                    <h1>This is a Header</h1>
                </header>
            </center>
        );
    }
});

Footer = React.createClass({
    render() {
        return (
            <center>
                <footer>
                    <h2>This is Footer</h2>
                </footer>
            </center>
        );
    }
});