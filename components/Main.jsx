/**
 * Created by Daniel on 2015/10/24.
 */

/**
 * Created by Daniel on 2015/10/24.
 */


var {
    Styles,
    FlatButton,
    RaisedButton,
    TextField
    } = MUI;

var { ThemeManager, LightRawTheme } = Styles;

injectTapEventPlugin();

// App component - represents the whole app
Main = React.createClass({

    childContextTypes: {
        muiTheme: React.PropTypes.object
    },

    getChildContext() {
        return {
            muiTheme: ThemeManager.getMuiTheme(LightRawTheme)
        };
    },

    // This mixin makes the getMeteorData method work
    mixins: [ReactMeteorData],

    // Loads items from the Tasks collection and puts them on this.data.tasks
    getMeteorData() {
        return {
        }
    },

    logoutHandler(event) {
        event.preventDefault();
        Accounts.logout();
        FlowRouter.reload();
    },

    render() {
        return (
            <div className="container">

                <RaisedButton label="Logout" onTouchTap={ this.logoutHandler } />
            </div>
        );
    }
});